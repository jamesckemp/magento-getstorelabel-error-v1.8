Description
===========

Fixes the following error when use getPriceHtml() in Magento.

`Fatal error: Call to a member function getStoreLabel() on a non-object in /app/design/frontend/default/default/template/catalog/product/price.phtml on line 50`

Installation
============

To install, download the *price.phtml* file in this repositiory and add it to:

`/app/design/frontend/yourpackage/yourtheme/template/catalog/product/price.phtml`